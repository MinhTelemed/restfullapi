const express = require('express');
var app = express();
var bodyParser = require('body-parser');
const AccountModel = require('./models/connect')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

//register

app.post('/register', (req, res, next) => {
    var username = req.body.username
    var password = req.body.password

    AccountModel.findOne({
        username: username
    }).then(data => {
        if (data) {
            res.json('user da ton tai')
        } else {
            return AccountModel.create({
                username: username,
                password: password
            })
        }
    }).then(data => {
        res.json('tao tai khoan thanh cong')
    }).catch(err => {
        res.status(500).json('tao tai khoan that bai')
    })
})

//login
app.post('/login', (req, res, next) => {
    var username = req.body.username
    var password = req.body.password

    AccountModel.findOne({
        username: username,
        password: password
    }).then(data => {
        if (data) {
            res.json('dang nhap thanh cong')
        } else {
            res.status(300).json('Ten tai khoan hoac mat khau khong dung')
        }
    }).catch(err => {
        res.status(500).json('co loi tren server')
    })
})

var accountRouter = require('./routers/account')
app.use('/api/account/', accountRouter)

app.get('/', (req, res, next) => {
    res.json('Home')
})

app.listen(3000, () => {
    console.log(`start server 3000`)
});