const router = require('express').Router()
const AccountModel = require('../models/connect')

router.get('/', (req, res, next) => {
    AccountModel.find({
        //get all
    }).then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json('khong the lay data')
    })
})

router.post('/', (req, res, next) => {
    var username = req.body.username
    var password = req.body.password

    AccountModel.findOne({
        username: username
    }).then(data => {
        if (data) {
            res.json('user da ton tai')
        } else {
            return AccountModel.create({
                username: username,
                password: password
            })
        }
    }).then(data => {
        res.json('tao tai khoan thanh cong')
    }).catch(err => {
        res.status(500).json('tao tai khoan that bai')
    })
})

router.put('/:id', (req, res, next) => {
    var id = req.params.id
    var newPassword = req.body.newPassword
    console.log(newPassword);

    AccountModel.findByIdAndUpdate(id, {
        password: newPassword
    }).then(data => {
        res.json('sua tai khoan thanh cong')
    }).catch(err => {
        res.status(500).json('cap nhat tai khoan that bai')
    })
})

router.delete('/:id', (req, res, next) => {
    var id = req.params.id
    AccountModel.deleteOne({
        _id: id
    }).then(data => {
        res.json('xoa tai khoan thanh cong')
    }).catch(err => {
        res.status(500).json('xoa tai khoan that bai')
    })
})

router.get('/:id', (req, res, next) => {
    var id = req.params.id
    AccountModel.findById({
        _id: id
    }).then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json('khong tim thay tai khoan')
    })
})


module.exports = router